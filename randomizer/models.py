from django.contrib.auth.base_user import BaseUserManager
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.urls import reverse
from django.utils.text import slugify
from django.conf import settings
from django.utils.translation import gettext_lazy as _

import smallerize


# Create your models here.
class Study(models.Model):
    PLANNING_STATUS = 'planning'
    IN_PROGRESS_STATUS = 'in_progress'
    FINISHED_STATUS = 'finished'
    STATUS_CHOICES = (
        (PLANNING_STATUS, "Planning"),
        (IN_PROGRESS_STATUS, "In Progress"),
        (FINISHED_STATUS, "Finished")
    )

    name = models.CharField(
        max_length=100,
        blank=False,
        help_text=("Give each study a unique name so everyone working on them"
                   " can find it easily")
    )
    date_created = models.DateTimeField(
        'Date study was created',
        auto_now_add=True
    )
    status = models.CharField(
        max_length=15,
        choices=STATUS_CHOICES,
        default=PLANNING_STATUS
    )
    # NOTE: May be worth using a 'through' model here
    #   to track when people were added to the study
    admins = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        related_name='admin_studies'
    )
    allocators = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        related_name='allocator_studies'
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'studies'

    def get_absolute_url(self):
        return reverse('study_detail', args=[self.id])

    def get_n_arms(self):
        return self.arms.count()

    # TODO: See if we can optimise this a bit with select_related,
    #   would be good to not have to do two db calls for
    #   factors and arms?
    def to_smallerize_minimizer(
            self,
            add_existing_participants=False) -> smallerize.Minimizer:
        """
        Create a ``smallerize.Minimizer`` object from the current
        study, including all the factors and arms, and
        the minimization parameters that have been set.

        :param add_existing_participants: If ``True``, all the currently
        allocated participants will be added to the ``Minimizer`` object,
        so that future allocations take them into account. If ``False``,
        just create the ``Minimizer`` but leave all the counts at zero.
        """
        db_factors = self.factors.prefetch_related('levels').all()
        factors = [f.to_smallerize_factor() for f in db_factors]
        arms = [a.to_smallerize_arm() for a in self.arms.all()]
        minimizer_kwargs = self.config.get().get_smallerize_kwargs()
        study = smallerize.Minimizer(
            factors=factors,
            arms=arms,
            **minimizer_kwargs
        )

        if add_existing_participants:
            factor_levels = self.get_allocation_factor_levels()
            for participant, levels in factor_levels.items():
                arm = levels.pop('__arm__')
                study.add_existing_participant(
                    factor_levels=levels,
                    arm=arm
                )

        return study

    def get_allocation_factor_levels(self) -> dict:
        """
        Get all the ``Allocation``s for the current study, and
        return them as dict of dicts showing the factor levels
         and assigned arms for each participant, like:

        ``{'id1': {'factor1': 'high', 'factor2': 'C',
                   '__arm__': 'Active'},
           'id2': {'factor1': 'low', 'factor2': 'A',
                   '__arm__': 'Control'}}``

        These can be passed to a ``smallerize.Minimizer``
        object (after popping out the '__arm__' value)
        """
        # level_data: each participant has multiple dicts, one
        #   for each factor
        level_data = Allocation.objects.filter(study=self.id)\
            .values('id_code', 'levels__factor__slug',
                    'levels__name', 'arm__name')

        # Combine into a single dict for each participant
        levels_collapsed = {}
        for row in level_data:
            current_id_code = row['id_code']
            row_factor = row['levels__factor__slug']
            row_level = row['levels__name']
            # New participant: get factor and arm
            if current_id_code not in levels_collapsed:
                levels_collapsed[current_id_code] = {
                    row_factor: row_level,
                    # Protect 'arm' with double underscores, to ensure
                    #   it doesn't collide with any user-created
                    #   names for factors
                    '__arm__': row['arm__name']
                }
            # Existing participant: just get factor
            else:
                levels_collapsed[current_id_code][row_factor] = row_level
        return levels_collapsed


class MinimConfig(models.Model):
    STANDARD_DEVIATION_D = 'standard_deviation'
    VARIANCE_D = 'variance'
    RANGE_D = 'range'
    OVER_MAX_RANGE_D = 'over_max_range'
    IS_LARGEST_D = 'is_largest'
    MARGINAL_BALANCE_D = 'marginal_balance'
    D_IMBALANCE_CHOICES = (
        (STANDARD_DEVIATION_D, 'Standard deviation'),
        (VARIANCE_D, 'Variance'),
        (RANGE_D, 'Range'),
        (OVER_MAX_RANGE_D, 'Range exceeds threshold'),
        (IS_LARGEST_D, 'Current arm is the largest'),
        (MARGINAL_BALANCE_D, 'Marginal balance method')
    )

    SUM_TOTAL = 'sum'
    WEIGHTED_SUM_TOTAL = 'weighted_sum'
    TOTAL_IMBALANCE_CHOICES = (
        (SUM_TOTAL, 'Sum'),
        (WEIGHTED_SUM_TOTAL, 'Weighted sum')
    )

    BEST_ONLY_PROB = 'best_only'
    RANK_ALL_PROB = 'rank_all'
    PURE_RANDOM_PROB = 'pure_random'
    BIASED_COIN_PROB = 'biased_coin'
    PROBABILITY_METHOD_CHOICES = (
        (BEST_ONLY_PROB, 'Best arm only'),
        (RANK_ALL_PROB, 'Assign by rank'),
        (PURE_RANDOM_PROB, 'Pure random (no minimization)'),
        (BIASED_COIN_PROB, 'Biased coin method')
    )

    study = models.ForeignKey(Study,
                              on_delete=models.CASCADE,
                              related_name='config')

    d_imbalance_method = models.CharField(
        max_length=30,
        choices=D_IMBALANCE_CHOICES,
        default=STANDARD_DEVIATION_D,
        blank=False,
        verbose_name='Factor imbalance method',
        help_text='How imbalance within each factor is calculated'
    )

    total_imbalance_method = models.CharField(
        max_length=30,
        choices=TOTAL_IMBALANCE_CHOICES,
        default=SUM_TOTAL,
        blank=False,
        verbose_name='Total imbalance method',
        help_text=('How the imbalance scores within each factor are combined.'
                   ' Use "Weighted sum" if factors have different importance'
                   ' weights')
    )

    probability_method = models.CharField(
        max_length=30,
        choices=PROBABILITY_METHOD_CHOICES,
        default=BEST_ONLY_PROB,
        blank=False,
        verbose_name='Probability method',
        help_text='How probabilities are assigned to arms, based on imbalance'
    )

    d_max_range = models.PositiveIntegerField(
        'Range threshold',
        default=1,
        blank=True,
        null=True,
        help_text="The maximum range of imbalance between arms allowed"
    )

    preferred_p = models.FloatField(
        'Preferred arm probability',
        default=0.7,
        blank=True,
        null=True,
        validators=[
            MinValueValidator(
                0,
                message='Probability must be between 0.0 and 1.0'
            ),
            MaxValueValidator(
                1,
                message='Probability must be between 0.0 and 1.0'
            )
        ],
        help_text=("The probability that the arm that produces the lowest"
                   " imbalance will be assigned")
    )

    q = models.FloatField(
        'q parameter',
        default=0.5,
        blank=True,
        null=True,
        validators=[
            MinValueValidator(
                0,
                message='q must be between 0.0 and 1.0'
            ),
            MaxValueValidator(
                1,
                message='q must be between 0.0 and 1.0'
            )
        ],
    )

    def get_smallerize_kwargs(self) -> dict:
        # Basic config
        config = {
            'd_imbalance_method': self.d_imbalance_method,
            'total_imbalance_method': self.total_imbalance_method,
            'probability_method': self.probability_method
        }

        # Add extra required args for specific methods
        if self.d_imbalance_method == self.OVER_MAX_RANGE_D:
            config['d_max_range'] = self.d_max_range

        if self.probability_method in (self.BEST_ONLY_PROB,
                                       self.BIASED_COIN_PROB):
            config['preferred_p'] = self.preferred_p

        if self.probability_method == self.RANK_ALL_PROB:
            config['q'] = self.q

        return config


class Arm(models.Model):
    study = models.ForeignKey(
        Study, on_delete=models.CASCADE, related_name='arms'
    )
    name = models.CharField(max_length=100, blank=False)
    description = models.TextField(blank=True)
    ratio = models.PositiveIntegerField(
        'Allocation ratio',
        default=1,
        validators=[MinValueValidator(1, message='The ratio must be >= 1')],
        help_text=("Allocation ratio, as an integer. If all arms will have"
                   " equal numbers of participants, leave this as 1")
    )

    class Meta:
        unique_together = (('study', 'name'),)

    def __str__(self):
        return self.name

    def to_smallerize_arm(self) -> smallerize.Arm:
        arm = smallerize.Arm(
            name=self.name,
            allocation_ratio=self.ratio
        )
        return arm


class Factor(models.Model):
    study = models.ForeignKey(Study, on_delete=models.CASCADE,
                              related_name='factors')
    name = models.CharField(max_length=100, blank=False)
    description = models.TextField(blank=True)
    weight = models.PositiveIntegerField(
        blank=False,
        default=1,
        validators=[MinValueValidator(1, message='The weight must be >= 1')],
        help_text=('Optional importance weight for the factor, if'
                   ' some factors are more important than others.'
                   ' Leave at 1 if all factors are equally important')
    )
    slug = models.SlugField(
        max_length=50,
        blank=False
    )

    class Meta:
        unique_together = (('study', 'name'), ('study', 'slug'))

    def __str__(self):
        return self.name

    # Add the slug upon saving
    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.name)

        super(Factor, self).save(*args, **kwargs)

    def to_smallerize_factor(self) -> smallerize.Factor:
        factor = smallerize.Factor(
            name=self.slug,
            levels=list(str(level) for level in self.levels.all()),
            weight=self.weight
        )
        return factor


class Level(models.Model):
    factor = models.ForeignKey(Factor, on_delete=models.CASCADE,
                               related_name='levels')
    name = models.CharField(max_length=100, blank=False)
    description = models.TextField(max_length=200, blank=True)

    class Meta:
        unique_together = (('factor', 'name'),)

    def __str__(self):
        return self.name


class Allocation(models.Model):
    study = models.ForeignKey(Study, on_delete=models.CASCADE,
                              related_name='allocations')
    allocated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
        related_name='allocations'
    )
    date_assigned = models.DateTimeField(
        'Date initial assignment was performed',
        auto_now_add=True
    )
    id_code = models.CharField('Participant ID code',
                               blank=False, max_length=100)
    slug = models.SlugField(
        max_length=50,
        blank=False
    )
    levels = models.ManyToManyField(Level, related_name='allocations')
    arm = models.ForeignKey(Arm, on_delete=models.CASCADE)
    comments = models.TextField('Any comments about this participant',
                                blank=True, max_length=1000)

    class Meta:
        unique_together = (('study', 'id_code'), ('study', 'slug'))

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.id_code)

        super(Allocation, self).save(*args, **kwargs)


class UserManager(BaseUserManager):

    def create_user(self, email, first_name, last_name, password=None,
                    **extra_fields):
        if not email:
            raise ValueError('The Email must be set')
        email = self.normalize_email(email)
        user = self.model(
            email=email,
            first_name=first_name,
            last_name=last_name,
            **extra_fields
        )
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, first_name, last_name, password):
        user = self.create_user(
            email, first_name, last_name, password,
            is_superuser=True,
            is_staff=True
        )
        return user


# TODO: Get this working in the Django admin view, see:
#  https://docs.djangoproject.com/en/2.1/topics/auth/customizing/#a-full-example
class User(AbstractBaseUser, PermissionsMixin):
    """User model that uses email as the username."""
    username = None
    email = models.EmailField('Email address', unique=True, blank=False)
    first_name = models.CharField(_('first name'), max_length=30, blank=False)
    last_name = models.CharField(_('last name'), max_length=150, blank=False)
    is_active = models.BooleanField(_('active'), default=True)
    is_staff = models.BooleanField(_('staff'), default=False)

    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    objects = UserManager()
