from django.contrib import admin

from .models import Allocation, Arm, Factor, Level, Study, MinimConfig

# Register your models here.
admin.site.register(Allocation)
admin.site.register(Arm)
admin.site.register(Factor)
admin.site.register(Level)
admin.site.register(Study)
admin.site.register(MinimConfig)
