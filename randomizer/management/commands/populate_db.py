import random

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model
from tests import factories


class Command(BaseCommand):
    help = 'Create example studies to populate the database,' \
           'with varying numbers of allocated participants'

    def add_arguments(self, parser):
        parser.add_argument('--studies',
                            default=10,
                            type=int,
                            help='The number of studies to create.')

    def handle(self, *args, **options):
        users = [factories.UserFactory() for _ in range(10)]
        User = get_user_model()
        superuser = User.objects.filter(is_superuser=True).first()
        print("Superuser:", superuser)

        factor_names = ['Gender', 'Site', 'Severity', 'Age']
        levels = {
            'Gender': ['Male', 'Female', 'Other'],
            'Site': ['Site A', 'Site B', 'Site C', 'Site D'],
            'Severity': ['Low', 'High'],
            'Age': ['Young', 'Old']
        }

        for study_num in range(options['studies']):
            current_admins = random.sample(users, 3) + [superuser]
            current_allocators = random.sample(users, 3)
            study = factories.StudyFactory(
                admins=current_admins,
                allocators=current_allocators
            )

            n_arms = random.randint(2, 3)
            current_arms = [factories.ArmFactory(study=study)
                            for _ in range(n_arms)]

            n_factors = random.randint(1, len(factor_names))
            current_factors = random.sample(factor_names, n_factors)
            for name in current_factors:
                factor = factories.FactorFactory(
                    study=study,
                    name=name
                )

                for level_name in levels[name]:
                    level = factories.LevelFactory(
                        name=level_name,
                        factor=factor
                    )

            n_allocations = random.randint(0, 20)
            for allocation_num in range(n_allocations):
                arm = random.choice(current_arms)
                user = random.choice(current_allocators)

                current_levels = []
                factors = study.factors.all()
                for factor in factors:
                    current_levels.append(random.choice(factor.levels.all()))

                allocation = factories.AllocationFactory(
                    study=study,
                    allocated_by=user,
                    arm=arm,
                    levels=current_levels
                )
