from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.forms import (ModelForm, NumberInput, TextInput, Form,
                          ChoiceField, BooleanField, EmailField,
                          FloatField,
                          inlineformset_factory, HiddenInput,
                          ValidationError)

import crispy_forms.helper
import crispy_forms.layout

from .models import Allocation, Arm, Factor, Level, MinimConfig, Study


class ArmForm(ModelForm):

    class Meta:
        model = Arm
        fields = ['name', 'description', 'ratio']
        widgets = {
            'description': TextInput,
            # Need a data-default-value field for the FormsetManager
            #   javascript to work
            'ratio': NumberInput(
                attrs={'data-default-value': 1, 'min': 1}
            )
        }


"""
Formset used within the create study form
"""
ArmFormSet = inlineformset_factory(
    Study,
    Arm,
    form=ArmForm,
    extra=0,
    min_num=2,
    validate_min=True,
    can_delete=False
)


class FactorForm(ModelForm):

    class Meta:
        model = Factor
        fields = ('name', 'description', 'weight')
        widgets = {
            'description': TextInput,
            'weight': NumberInput(
                attrs={'min': 1}
            )
        }


class LevelForm(ModelForm):

    class Meta:
        model = Level
        fields = ['name', 'description']
        widgets = {
            'description': TextInput,
        }

"""
Formset used within the create factor form
"""
LevelFormSet = inlineformset_factory(
    Factor,
    Level,
    form=LevelForm,
    min_num=2,
    extra=0,
    validate_min=True,
    can_delete=False
)


class AllocationForm(ModelForm):

    class Meta:
        model = Allocation
        fields = ['id_code', 'comments']

    def __init__(self, study, *args, **kwargs):
        super(AllocationForm, self).__init__(*args, **kwargs)
        self.study = study
        self.factors = self.study.factors.all()
        for factor in self.factors:
            levels = factor.levels.all()
            self.fields[factor.name] = ChoiceField(
                choices = tuple((l.name, l.name) for l in levels)
            )


class UserRegistrationForm(UserCreationForm):
    email = EmailField(
        label="Email (this will be your username)",
        help_text='Email is required. Please provide a valid email address.'
    )
    helper = crispy_forms.helper.FormHelper()
    helper.add_input(crispy_forms.layout.Submit('submit', 'Register'))

    class Meta:
        model = get_user_model()
        fields = ('email', 'first_name', 'last_name', 'password1', 'password2')


class MinimConfigForm(ModelForm):
    helper = crispy_forms.helper.FormHelper()
    helper.form_id = "config_form"
    # TODO: this still has class btn-primary, not sure how to fully
    #   override that?
    helper.add_input(
        crispy_forms.layout.Submit('submit', 'Save settings',
                                   css_class='btn btn-success')
    )

    class Meta:
        model = MinimConfig
        fields = ['d_imbalance_method', 'd_max_range',
                  'total_imbalance_method',
                  'probability_method', 'preferred_p', 'q']
        # Generic defaults, will be updated using JS in the actual view
        widgets = {
            'preferred_p': NumberInput(attrs={
                'min': 0,
                'max': 1,
                'step': 0.05
            }),
            'q': NumberInput(attrs={
                'min': 0,
                'max': 2,
                'step': 0.05
            })
        }

    def __init__(self, study, *args, **kwargs):
        super(MinimConfigForm, self).__init__(*args, **kwargs)
        self.study = study

    def clean_preferred_p(self):
        preferred_prob_methods = (MinimConfig.BEST_ONLY_PROB,
                                  MinimConfig.BIASED_COIN_PROB)
        if self.data['probability_method'] in preferred_prob_methods:
            N = self.study.get_n_arms()
            p = float(self.data['preferred_p'])
            p_valid = (1 / N < p) and (p <= 1.0)
            if not p_valid:
                error_str = (
                    "Preferred arm probability for a trial with {n} arms" +
                    " must be between 1/{n} and 1.0 (must be " +
                    "*greater than* 1/{n})"
                )
                raise ValidationError(error_str.format(n=N))
        return self.data['preferred_p']

    def clean_q(self):
        if self.data['probability_method'] == MinimConfig.RANK_ALL_PROB:
            N = self.study.get_n_arms()
            q = float(self.data['q'])
            q_valid = (1 / N < q) and (q < (2 / (N - 1)))
            if not q_valid:
                error_str = (
                        "q for a trial with {n} arms" +
                        " must be between 1/{n} and 2 / {n_minus1}"
                )
                raise ValidationError(error_str.format(n=N, n_minus1=N - 1))
        return self.data['q']

    def clean(self):
        """
        Set parameters that are not needed under the current settings
        to None
        """
        data = self.cleaned_data
        if data['d_imbalance_method'] != MinimConfig.OVER_MAX_RANGE_D:
            data['d_max_range'] = None

        preferred_prob_methods = (MinimConfig.BEST_ONLY_PROB,
                                  MinimConfig.BIASED_COIN_PROB)
        if data['probability_method'] not in preferred_prob_methods:
            data['preferred_p'] = None
        if data['probability_method'] != MinimConfig.RANK_ALL_PROB:
            data['q'] = None

        return super().clean()

    def save(self, commit=True):
        instance = super(MinimConfigForm, self).save(commit=False)
        instance.study = self.study
        if commit:
            instance.save()
        return instance
