'use strict';

class FormsetManager {

    constructor(formset_element, form_prefix, field_names) {
        this.form_prefix = form_prefix;
        this.element = formset_element;
        this.fields = field_names;
    }

    // Change the TOTAL_FORMS value by n
    // n = 1: add 1, n = -1: subtract 1
    add_to_total(n) {
        var total_forms = $(this.element).find("#" + this.form_prefix + "-TOTAL_FORMS");
        var current_val = parseInt(total_forms.attr("value"));
        total_forms.val(current_val + n);
    }

    get_total() {
        var total_forms = $(this.element).find("#" + this.form_prefix + "-TOTAL_FORMS");
        return(parseInt(total_forms.val()));
    }

    get_nth_fieldset(n) {
        var field_divs = {};
        this.fields.map(field_name => {
            var div_id = "#div_" + this.form_prefix + "-" + n.toString() + "-" + field_name;
            var div = $(this.element).find(div_id).first();
            field_divs[field_name] = div;
        });
        return(field_divs);
    }

    remove_one_fieldset() {
        var current_total = this.get_total();
        var last_fieldnum = current_total - 1;
        var last_fields = this.get_nth_fieldset(last_fieldnum);
        $.map(last_fields, (field, field_name) =>
            field.fadeOut(300, function() { $(this).remove(); })
        );
        this.add_to_total(-1);
    }

    add_extra_fieldset() {
        var current_total = this.get_total();
        // Fields are zero-indexed so this is actually the same as the old total
        var new_fieldnum = current_total;
        var zero_fields = this.get_nth_fieldset(0);
        var cloned_fields = {};
        $.map(zero_fields, (field, field_name) => cloned_fields[field_name] = field.clone());

        $.map(cloned_fields, (field_div, field_name) => {
            var field_tag = this.form_prefix + "-" + new_fieldnum + "-" + field_name;
            field_div.attr("id", "div_" + field_tag);

            // Update label attributes
            field_div.find("label").attr("for", field_tag);

            // Update input attributes, reset values to zero
            var input_els = field_div.find("input");
            field_div.find('input[type=text]').val(null);
            // NOTE: you must set a data-default-value on all number fields when
            //   using the formset manager, otherwise cloned fields keep
            //   the current input value
            var number_inputs = field_div.find('input[type=number]');
            number_inputs.val(function(i, value) {
                return $(this).attr("data-default-value");
            });
            input_els.attr("name", field_tag);
            input_els.attr("id", field_tag);

            // Add, with animation
            $(field_div).hide().appendTo(this.element).fadeIn(500);

        });

        // Increment TOTAL_FORMS counter
        this.add_to_total(1);
    }
}