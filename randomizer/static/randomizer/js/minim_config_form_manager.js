function set_preferred_p_attrs(n_arms) {
    let min_p = 1 / n_arms;
    // Use half-way point as default;
    let default_p = (1 + min_p) / 2;

    $("#id_preferred_p").attr({
        'value': default_p,
        'min': min_p,
        'max': 1
    });
}

function set_q_attrs(n_arms) {
    let min_q = 1 / n_arms;
    let max_q = 2 / (n_arms - 1);
    let default_q = (min_q + max_q) / 2;
    $("#id_q").attr({
        'value': default_q,
        'min': min_q,
        'max': max_q
    });
}

function set_up_minim_config_form(n_arms) {
    set_preferred_p_attrs(n_arms);
    set_q_attrs(n_arms);

    update_minim_config_form(n_arms)
}

function update_minim_config_form(n_arms) {
    let current_d_method = $("#id_d_imbalance_method").val();
    console.log(current_d_method);

    // d_max_range param only needed if using 'over_max_range'
    let max_range_div = $("#div_id_d_max_range");
    if (current_d_method === "over_max_range") {
        max_range_div.show();
    } else {
        max_range_div.hide();
    }

    let current_prob_method = $("#id_probability_method").val();

    // preferred_p needed if using best_only or biased_coin
    let needs_preferred_p = (current_prob_method === "best_only") || (current_prob_method === "biased_coin");
    let preferred_p_div = $("#div_id_preferred_p");
    if (needs_preferred_p) {
        preferred_p_div.show();
    } else {
        preferred_p_div.hide();
    }

    let needs_q = current_prob_method === "rank_all";
    let q_div = $("#div_id_q");
    if (needs_q) {
        q_div.show();
    } else {
        q_div.hide();
    }
}
