import json
import io
from functools import reduce

from crispy_forms import helper
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.models import Q, Count, Exists, Case, When, Value, BooleanField, \
    OuterRef
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views import View
from django.views.generic import (TemplateView, DetailView, CreateView,
                                  UpdateView,FormView, ListView)

import pandas as pd

from .forms import ArmFormSet, FactorForm, LevelFormSet, AllocationForm, \
    UserRegistrationForm, MinimConfigForm
from .models import Study, Factor, Level, Allocation, Arm, MinimConfig


# TODO: manage study access permissions with UserPassesTestMixin
# Create your views here.


class LandingView(TemplateView):
    """
    Landing page giving info about what the app does
    and directing users to log in/register
    """
    template_name = 'randomizer/landing_page.html'


class StudyDetailView(LoginRequiredMixin, UserPassesTestMixin,
                      DetailView):
    """
    List all info about a study, including arms, factors,
    allocators, admins etc.
    """
    model = Study
    # Using prefetch/annotate to reduce number of queries here,
    #   may be useful elsewhere
    # NOTE: see if we can get Exists to work for the config check
    queryset = Study.objects.prefetch_related(
        'admins',
        'allocators',
        'arms',
        'factors',
        'factors__levels') \
        .annotate(
            n_assigned=Count('allocations', distinct=True),
            has_config=Exists(
                MinimConfig.objects
                    .filter(study=OuterRef('id'))
                    .only('id')
            )
        )
    context_object_name = 'study'

    def test_func(self):
        study = Study.objects.filter(id=self.kwargs['pk'])\
            .prefetch_related('admins', 'allocators')\
            .get()
        is_admin = self.request.user in study.admins.all()
        is_allocator = self.request.user in study.allocators.all()
        return is_admin or is_allocator

    # Do required lookups in the view so we don't do additional
    #   lookups in the template
    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        return context

    def get_permission_denied_message(self):
        return ("You don't have permission to view this study. A study"
                " admin can give you permission.")

    def handle_no_permission(self):
        messages.error(
            self.request,
            self.get_permission_denied_message()
        )
        return super(StudyDetailView, self).handle_no_permission()


class StudyListView(ListView):
    model = Study
    context_object_name = 'studies'

    def get_queryset(self):
        admin = self.request.user.admin_studies.all()
        allocator = self.request.user.allocator_studies.all()
        return admin.union(allocator).prefetch_related('admins', 'allocators')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(StudyListView, self).get_context_data()
        context['user'] = self.request.user
        return context

class StudyCreateView(LoginRequiredMixin, CreateView):
    """
    Create a study, along with its arms.
    """
    template_name = 'randomizer/create_study.html'
    success_url = '/studies/{id}/'
    model = Study
    fields = ('name',)

    def handle_no_permission(self):
        """Add error message."""
        messages.error(self.request, self.get_permission_denied_message())
        response = super(StudyCreateView, self).handle_no_permission()
        return response

    def get_permission_denied_message(self):
        return "You must be logged in to create a new study."

    def get_form(self, form_class=None):
        """
        Add a crispy_forms helper so we can disable the form
        tag, we need a form tag in the template that covers
        both the study and arm forms.
        """
        form = super().get_form(form_class)
        form.helper = helper.FormHelper()
        form.helper.form_tag = False
        return form

    def get(self, request, *args, **kwargs):
        """
        Handles GET requests and instantiates blank versions of the form
        and its inline formsets.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        arm_formset = ArmFormSet(auto_id=True, prefix='arm')
        return self.render_to_response(
            self.get_context_data(form=form,
                                  arm_formset=arm_formset)
        )

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance and its inline
        formsets with the passed POST variables and then checking them for
        validity.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        arm_formset = ArmFormSet(self.request.POST, auto_id=True,
                                 prefix='arm')
        if form.is_valid() and arm_formset.is_valid():
            return self.form_valid(form, arm_formset)
        else:
            return self.form_invalid(form, arm_formset)

    def get_context_data(self, **kwargs):
        """
        Add a FormHelper to the arm_formset so we can disable the form
        tag.
        """
        context = super(StudyCreateView, self).get_context_data(**kwargs)
        context['arm_formset_helper'] = helper.FormHelper()
        context['arm_formset_helper'].form_tag = False
        return context

    def form_valid(self, form, arm_formset):
        """
        Check that both the study and arm forms are valid before
        saving.
        """
        self.object = form.save()
        # Add the current user to the admin list
        self.object.admins.add(self.request.user)
        arm_formset.instance = self.object
        arm_formset.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, arm_formset):
        return self.render_to_response(
            self.get_context_data(form=form,
                                  arm_formset=arm_formset)
        )


class FactorCreateView(LoginRequiredMixin, CreateView):
    """
    Create a factor alaong with its arms
    """
    template_name = 'randomizer/create_factor.html'
    success_url = '/studies/{study_id}/'
    model = Factor
    form_class = FactorForm

    def dispatch(self, request, *args, **kwargs):
        self.study = Study.objects.get(id=self.kwargs.get('study'))
        return super(FactorCreateView, self).dispatch(
            request, *args, **kwargs
        )

    def get_context_data(self, **kwargs):
        context = super(FactorCreateView, self).get_context_data(**kwargs)
        context['study'] = self.study
        context['level_formset_helper'] = helper.FormHelper()
        context['level_formset_helper'].form_tag = False
        return context

    def handle_no_permission(self):
        """Add error message."""
        messages.error(self.request, self.get_permission_denied_message())
        response = super(FactorCreateView, self).handle_no_permission()
        return response

    def get_permission_denied_message(self):
        return "You must be logged in to add a factor to a study."

    def get_form(self, form_class=None):
        """
        Add a crispy_forms helper so we can disable the form
        tag, we need a form tag in the template that covers
        both the study and arm forms.
        """
        form = super().get_form(form_class)
        form.helper = helper.FormHelper()
        form.helper.form_tag = False
        return form

    def get(self, request, *args, **kwargs):
        """
        Handles GET requests and instantiates blank versions of the form
        and its inline formsets.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        level_formset = LevelFormSet(auto_id=True, prefix='level')
        return self.render_to_response(
            self.get_context_data(form=form,
                                  level_formset=level_formset)
        )

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance and its inline
        formsets with the passed POST variables and then checking them for
        validity.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        level_formset = LevelFormSet(self.request.POST, auto_id=True,
                                     prefix='level')
        if form.is_valid() and level_formset.is_valid():
            return self.form_valid(form, level_formset)
        else:
            return self.form_invalid(form, level_formset)


    def form_valid(self, form, level_formset):
        """
        Check that both the study and level forms are valid before
        saving.
        """
        form.instance.study = self.study
        self.object = form.save()
        level_formset.instance = self.object
        level_formset.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, level_formset):
        return self.render_to_response(
            self.get_context_data(form=form,
                                  level_formset=level_formset)
        )


class AllocationCreateView(FormView):
    template_name = 'randomizer/allocate_new.html'
    form_class = AllocationForm

    def get_success_url(self):
        return reverse('study_detail', kwargs={'pk': self.study.id})

    def get_form(self, form_class=None):
        """
        Add a crispy_forms helper so we can disable the form
        tag.
        """
        form = super().get_form(form_class)
        form.helper = helper.FormHelper()
        form.helper.form_tag = False
        return form

    def dispatch(self, request, *args, **kwargs):
        self.study = Study.objects.get(id=self.kwargs.get('study'))
        self.user = self.request.user
        return super(AllocationCreateView, self).dispatch(
            request, *args, **kwargs
        )

    def get_context_data(self, **kwargs):
        context = super(AllocationCreateView, self).get_context_data(**kwargs)
        context['study'] = self.study
        return context

    def get_form_kwargs(self):
        kwargs = super(AllocationCreateView, self).get_form_kwargs()
        kwargs['study'] = self.study
        return kwargs

    def form_valid(self, form):
        minimizer = self.study.to_smallerize_minimizer(
            add_existing_participants=True
        )
        factor_levels = {
            factor.slug: form.cleaned_data[factor.name]
            for factor in form.factors
        }
        assigned_arm = minimizer.assign_participant(
            factor_levels=factor_levels
        )

        allocation = form.save(commit=False)
        allocation.study = self.study
        allocation.allocated_by = self.user
        allocation.arm = Arm.objects.get(name=assigned_arm, study=self.study)
        allocation.save()
        # NOTE: Think we need to save the allocation before we can
        #   link up the levels, not sure if there is any way around
        #   this?
        # Combine the queries for all levels into one so
        #   we only do one actual query
        level_qs = []
        for factor in form.factors:
            current_q = Q(
                name=form.cleaned_data[factor.name],
                factor=factor
            )
            level_qs.append(current_q)
        combined_q = reduce(lambda a, b: a | b, level_qs)
        levels = Level.objects.filter(combined_q)
        allocation.levels.add(*levels)

        messages.success(
            self.request,
            "Participant {id} successfully assigned!".format(
                id=allocation.id_code
            )
        )
        return HttpResponseRedirect(self.get_success_url())


class AllocationDetailView(DetailView):
    model = Allocation

    def dispatch(self, request, *args, **kwargs):
        self.study = get_object_or_404(Study, id=self.kwargs.get('study'))
        self.id_slug = self.kwargs.get('id_slug')
        return super(AllocationDetailView, self).dispatch(
            request, *args, **kwargs
        )

    def get_object(self, queryset=None):
        return get_object_or_404(Allocation, slug=self.id_slug,
                                 study=self.study)


class AllocationTableView(ListView):
    model = Allocation
    template_name = 'randomizer/allocation_table.html'
    context_object_name = 'allocations'
    ordering = 'date_assigned'
    paginate_by = 10

    def dispatch(self, request, *args, **kwargs):
        self.study = get_object_or_404(Study, id=self.kwargs.get('study'))
        return super(AllocationTableView, self).dispatch(
            request, *args, **kwargs
        )

    def get_queryset(self):
        return Allocation.objects.filter(study=self.study)\
            .select_related('study', 'allocated_by', 'arm')\
            .prefetch_related('levels', 'levels__factor')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(AllocationTableView, self).get_context_data(**kwargs)
        context['study'] = self.study
        # NOTE: Seemed to be necessary to do this separately (to
        #   get factor names) as otherwise we get errors about
        #   ordering a queryset after a slice
        context['first_allocation'] = self.get_queryset().first()
        return context


class AllocationTableDownloadView(View):
    """
    Return the allocation table as a downloadable excel file.
    """
    http_method_names = ['get']

    def get_allocations(self):
        allocations = Allocation.objects.filter(study=self.study)\
            .select_related('study', 'allocated_by', 'arm')\
            .prefetch_related('levels', 'levels__factor')\
            .all()\
            .values(
                'id_code',
                'allocated_by__email',
                'date_assigned',
                'arm__name',
                'levels__name',
                'levels__factor__name'
            )
        return allocations

    def get_data_frame(self, allocations):
        # Get all records and rename
        df = pd.DataFrame.from_records(allocations)\
            .rename(columns={
                'id_code': 'ParticipantId',
                'allocated_by__email': 'AllocatedBy',
                'date_assigned': 'DateAllocated',
                'arm__name': 'Arm',
                'levels__name': 'Level',
                'levels__factor__name': 'Factor'
            })

        # Reformat the date nicely
        df['DateAllocated'] = df['DateAllocated'].dt.strftime(
            '%Y-%m-%d %H:%M'
        )

        # Get everything except for levels/factors, these
        #   are repeated so drop dupes
        main = df.loc[
           :,
           ['ParticipantId', 'AllocatedBy', 'DateAllocated', 'Arm']
        ].drop_duplicates()\
            .set_index('ParticipantId')

        # Reshape factors/levels to wide format
        levels = df.loc[:, ['ParticipantId', 'Level', 'Factor']]\
            .pivot(index='ParticipantId',
                   columns='Factor',
                   values='Level')

        # Merge with the reset of the data
        main = main.join(levels).reset_index()

        return main

    def get(self, request, *args, **kwargs):
        excel_content_type = (
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        )

        self.study = get_object_or_404(Study, id=self.kwargs.get('study'))
        allocations = self.get_allocations()
        df = self.get_data_frame(allocations)

        output = io.BytesIO()
        writer = pd.ExcelWriter(output, engine='xlsxwriter')
        df.to_excel(writer, index=False, sheet_name='Allocations')
        self.resize_excel_columns(df, writer)
        writer.save()
        output.seek(0)

        response = HttpResponse(
            output,
            content_type=excel_content_type
        )
        disposition =  (
            'attachment; filename="{f_name}.xlsx"'.format(
                f_name='Allocations'
            )
        )
        response['Content-Disposition'] = disposition
        return response

    @staticmethod
    def resize_excel_columns(df: pd.DataFrame, excel_writer: pd.ExcelWriter):
        max_col_width = 50
        sheet = excel_writer.sheets['Allocations']

        for index, column in enumerate(df.columns):
            column_len = df[column].astype(str).str.len().max()
            column_len = max(column_len, len(column)) + 10
            # Set a max so we don't end up with overly wide
            #   columns
            column_len = min(column_len, max_col_width)
            sheet.set_column(index, index, column_len)


# TODO: Email confirmation
class UserCreationView(CreateView):
    template_name = 'registration/register.html'
    form_class = UserRegistrationForm
    success_url = '/accounts/login/'


class MinimConfigCreationView(CreateView):
    form_class = MinimConfigForm
    template_name = 'randomizer/create_minim_config.html'

    def dispatch(self, request, *args, **kwargs):
        self.study = get_object_or_404(Study, id=self.kwargs.get('study'))
        return super(MinimConfigCreationView, self).dispatch(
            request, *args, **kwargs
        )

    def get_form_kwargs(self):
        kwargs = super(MinimConfigCreationView, self).get_form_kwargs()
        kwargs['study'] = self.study
        return kwargs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(MinimConfigCreationView, self)\
            .get_context_data(**kwargs)
        context['study'] = self.study
        context['json_data'] = json.dumps({
            'n_arms': self.study.get_n_arms()
        })
        return context

    def get_success_url(self):
        return reverse('study_detail', kwargs={'pk': self.study.id})


class MinimConfigUpdateView(UpdateView):
    model = MinimConfig
    form_class = MinimConfigForm
    template_name = 'randomizer/update_minim_config.html'

    def dispatch(self, request, *args, **kwargs):
        self.study = get_object_or_404(Study, id=self.kwargs.get('study'))
        return super(MinimConfigUpdateView, self).dispatch(
            request, *args, **kwargs
        )

    def get_queryset(self):
        return MinimConfig.objects.filter(study=self.study)

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        obj = queryset.get()
        return obj

    def get_form_kwargs(self):
        kwargs = super(MinimConfigUpdateView, self).get_form_kwargs()
        kwargs['study'] = self.study
        return kwargs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(MinimConfigUpdateView, self) \
            .get_context_data(**kwargs)
        context['study'] = self.study
        context['json_data'] = json.dumps({
            'n_arms': self.study.get_n_arms()
        })
        return context

    def get_success_url(self):
        return reverse('study_detail', kwargs={'pk': self.study.id})
