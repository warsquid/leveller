import itertools

import pytest

from django.test import TestCase

from randomizer.forms import ArmFormSet, MinimConfigForm
from randomizer.models import Study, MinimConfig
from .factories import ArmFactory, StudyFactory


class TestArmFormSet(TestCase):

    def setUp(self):
        self.study = StudyFactory()

    def test_min_arms(self):
        """
        Check that the form requires 2 or more forms to be submitted
        """
        form = ArmFormSet(
            instance=self.study,
            prefix='arm',
            data={
                'arm-TOTAL_FORMS': '1',
                'arm-INITIAL_FORMS': '0',
                'arm-MIN_NUM_FORMS': '2',
                'arm-MAX_NUM_FORMS': '1000',
                'arm-0-name': 'Arm1',
                'arm-0-ratio': '1',
            }
        )
        self.assertFalse(form.is_valid())
        assert 'Please submit 2 or more forms.' in form.non_form_errors()

    def test_multiple_arms(self):
        form = ArmFormSet(
            instance=self.study,
            prefix='arm',
            data={
                'arm-TOTAL_FORMS': '3',
                'arm-INITIAL_FORMS': '0',
                'arm-MIN_NUM_FORMS': '2',
                'arm-MAX_NUM_FORMS': '1000',
                'arm-0-name': 'Arm0',
                'arm-0-ratio': '1',
                'arm-1-name': 'Arm1',
                'arm-1-ratio': '1',
                'arm-2-name': 'Arm2',
                'arm-2-ratio': '1',
                'arm-3-name': 'Arm3',
                'arm-3-ratio': '1'
            }
        )
        self.assertTrue(form.is_valid())


class TestMinimConfigForm(TestCase):

    def setUp(self):
        self.study = StudyFactory()
        arm1 = ArmFactory(study=self.study)
        arm2 = ArmFactory(study=self.study)

    def test_blank_values(self):
        """
        Check that we set unneeded values (preferred_p, q, d_max_range)
        to None when saving the form.
        """
        d_methods = [name for name, string in MinimConfig.D_IMBALANCE_CHOICES]
        total_methods = [
            name for name, string in MinimConfig.TOTAL_IMBALANCE_CHOICES
        ]
        prob_methods = [
            name for name, string in MinimConfig.PROBABILITY_METHOD_CHOICES
        ]
        all_combs = itertools.product(d_methods, total_methods, prob_methods)

        for d, total, prob in all_combs:
            form = MinimConfigForm(
                study=self.study,
                data=dict(
                    d_imbalance_method=d,
                    total_imbalance_method=total,
                    probability_method=prob,
                    d_max_range=1,
                    preferred_p=0.7,
                    q=0.5
                )
            )
            form.full_clean()

            if d == MinimConfig.OVER_MAX_RANGE_D:
                assert form.cleaned_data['d_max_range'] is not None
            else:
                assert form.cleaned_data['d_max_range'] is None
