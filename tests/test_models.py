from datetime import datetime
import itertools
import random

import pytest
from django.test import TestCase
from django.db.utils import IntegrityError

import smallerize

from randomizer.models import Arm, Factor, Study, MinimConfig
from .factories import (
    AllocationFactory, ArmFactory, FactorFactory, LevelFactory, UserFactory,
    StudyFactory, MinimConfigFactory
)


@pytest.fixture(scope='class')
def example_study_with_participants(request):
    # Slightly clunky to set up this example but very important
    #   to test this logic, so making it as explicit as possible
    study = StudyFactory()
    sex = FactorFactory(name='Sex', study=study)
    male = LevelFactory(factor=sex, name='Male')
    female = LevelFactory(factor=sex, name='Female')
    age = FactorFactory(name='Age', study=study)
    young = LevelFactory(name='Young', factor=age)
    old = LevelFactory(name='Old', factor=age)
    arm1 = ArmFactory(name='Arm1', study=study)
    arm2 = ArmFactory(name='Arm2', study=study)
    # Set config after adding arms so we know for calculating
    #   q
    config = MinimConfigFactory(study=study)

    all_combinations = itertools.product([male, female], [young, old])
    for current_sex, current_age in all_combinations:
        allocation = AllocationFactory(
            study=study,
            arm=arm1 if current_sex.name == 'Female' else arm2,
            levels=[current_sex, current_age]
        )

    request.cls.example_study_with_participants = study


@pytest.mark.usefixtures("example_study_with_participants")
class TestStudy(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestStudy, cls).setUpClass()
        cls.user1 = UserFactory()
        cls.user2 = UserFactory()

    def test_name_needed(self):
        with self.assertRaises(IntegrityError):
            Study.objects.create(name=None)

    def test_date_created(self):
        now = datetime.now()
        study = Study.objects.create(name='Study 1')
        assert study.date_created.date() == now.date()

    def test_default_status(self):
        study = StudyFactory()
        assert study.status == Study.PLANNING_STATUS

    def test_add_admin(self):
        # Test adding admin directly on creation and afterwards
        study = StudyFactory(admins=[self.user1])
        assert self.user1 in study.admins.all()
        assert study in self.user1.admin_studies.all()
        study.admins.add(self.user2)
        assert self.user2 in study.admins.all()
        assert study in self.user2.admin_studies.all()

    def test_add_allocator(self):
        study = StudyFactory(allocators=[self.user1])
        assert self.user1 in study.allocators.all()
        assert study in self.user1.allocator_studies.all()
        study.allocators.add(self.user2)
        assert self.user2 in study.allocators.all()
        assert study in self.user2.allocator_studies.all()

    def test_str(self):
        study = StudyFactory(name='Study 1')
        assert str(study) == 'Study 1'

    def test_to_minimizer(self):
        study = StudyFactory()
        factors = FactorFactory.create_batch(2, study=study)
        created_levels = {}
        for factor in factors:
            created_levels[factor.slug] = LevelFactory.create_batch(
                2,
                factor=factor
            )
        arms = ArmFactory.create_batch(2, study=study)
        # Set config after setting arms
        config = MinimConfigFactory(study=study)

        # TODO: test number of queries here?
        minimizer = study.to_smallerize_minimizer()
        for factor in factors:
            assert factor.slug in minimizer.factor_names
            index = minimizer.factor_names.index(factor.slug)
            for level in created_levels[factor.slug]:
                assert level.name in minimizer.factors[index].levels
        for arm in arms:
            assert arm.name in minimizer.arm_names

    def test_to_minimizer_with_participants(self):
        study = self.example_study_with_participants
        # TODO: Test number of queries here?
        minimizer = study.to_smallerize_minimizer(
            add_existing_participants=True
        )
        counts = minimizer.get_current_x_counts(
            {'sex': 'Female', 'age': 'Young'}
        )
        assert counts['sex']['Arm1'] == 2
        assert counts['sex']['Arm2'] == 0
        assert counts['age']['Arm1'] == 1
        assert counts['age']['Arm2'] == 1

    def test_get_allocation_factor_levels(self):
        study = self.example_study_with_participants
        with self.assertNumQueries(1):
            tab = study.get_allocation_factor_levels()
        factor_levels = [levels for id_code, levels in tab.items()]
        n_female = sum(p['sex'] == 'Female' for p in factor_levels)
        assert n_female == 2
        n_female_arm1 = sum(
            (p['sex'] == 'Female') & (p['__arm__'] == 'Arm1')
            for p in factor_levels
        )
        assert n_female_arm1 == 2
        n_young = sum(p['age'] == 'Young' for p in factor_levels)
        assert n_young == 2
        n_female_old = sum(
            (p['sex'] == 'Female') & (p['age'] == 'Old')
            for p in factor_levels
        )
        assert n_female_old == 1


class TestMinimConfig(TestCase):
    @staticmethod
    def _get_keys(choices):
        """
        Get the keys from a sequence of (key, label) tuples (used to
        store the available choices for a model field)
        """
        return [key for key, label in choices]

    def test_methods_match(self):
        d_method_names = self._get_keys(MinimConfig.D_IMBALANCE_CHOICES)
        assert all(name in smallerize.Minimizer.D_IMBALANCE_METHODS
                   for name in d_method_names)

        total_method_names = self._get_keys(MinimConfig.TOTAL_IMBALANCE_CHOICES)
        assert all(name in smallerize.Minimizer.TOTAL_IMBALANCE_METHODS
                   for name in total_method_names)

        prob_method_names = self._get_keys(
            MinimConfig.PROBABILITY_METHOD_CHOICES
        )
        assert all(name in smallerize.Minimizer.PROBABILITY_METHODS
                   for name in prob_method_names)

    def test_get_smallerize_kwargs(self):
        """
        Test all possible options for each arg to make sure we
        convert to the right set of Minimizer args in every case.
        """
        d_method_names = self._get_keys(MinimConfig.D_IMBALANCE_CHOICES)
        total_method_names = self._get_keys(MinimConfig.TOTAL_IMBALANCE_CHOICES)
        prob_method_names = self._get_keys(
            MinimConfig.PROBABILITY_METHOD_CHOICES
        )

        all_combs = itertools.product(
            d_method_names,
            total_method_names,
            prob_method_names
        )
        for d_method, total_method, prob_method in all_combs:
            config_in = {
                'd_imbalance_method': d_method,
                'total_imbalance_method': total_method,
                'probability_method': prob_method
            }
            if d_method == MinimConfig.OVER_MAX_RANGE_D:
                config_in['d_max_range'] = random.randint(0, 5)

            if prob_method in (MinimConfig.BEST_ONLY_PROB,
                               MinimConfig.BIASED_COIN_PROB):
                config_in['preferred_p'] = random.random()
            if prob_method == MinimConfig.RANK_ALL_PROB:
                config_in['q'] = random.random()
            instance = MinimConfig(**config_in)
            config_out = instance.get_smallerize_kwargs()
            assert config_in == config_out


class TestArm(TestCase):

    def setUp(self):
        super(TestArm, self).setUp()
        self.study = StudyFactory()

    def test_unique_within_study(self):
        with pytest.raises(IntegrityError) as except_info:
            arm1 = Arm.objects.create(name='Same', study=self.study)
            arm2 = Arm.objects.create(name='Same', study=self.study)
            assert 'UNIQUE' in except_info.value

    def test_allocation_ratio_default(self):
        arm = ArmFactory(study=self.study)
        assert arm.ratio == 1

    def test_to_smallerize_arm(self):
        arm = ArmFactory(study=self.study)
        sm_arm = arm.to_smallerize_arm()
        assert isinstance(sm_arm, smallerize.Arm)
        assert sm_arm.name == arm.name
        assert sm_arm.allocation_ratio == arm.ratio

    def test_to_smallerize_nondefault_ratio(self):
        arm = ArmFactory(study=self.study, ratio=5)
        sm_arm = arm.to_smallerize_arm()
        assert sm_arm.allocation_ratio == 5


class TestFactor(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestFactor, cls).setUpClass()
        cls.study = StudyFactory()

    def test_unique_within_study(self):
        with pytest.raises(IntegrityError) as except_info:
            factor1 = Factor.objects.create(name='Same', study=self.study)
            factor2 = Factor.objects.create(name='Same', study=self.study)
            assert 'UNIQUE' in except_info.value

    def test_slug(self):
        factor = FactorFactory(
            name='Disease Severity',
            study=self.study
        )
        assert factor.slug == 'disease-severity'

        # slug should not change after name change
        factor.name = 'DiseaseBadness'
        factor.save()
        assert factor.slug == 'disease-severity'

    def test_to_smallerize(self):
        factor = FactorFactory(study=self.study)
        levels = LevelFactory.create_batch(2, factor=factor)
        sm_factor = factor.to_smallerize_factor()

        assert sm_factor.name == factor.slug
        for level in levels:
            assert level.name in sm_factor.levels
        assert sm_factor.weight == factor.weight

    def test_capitalized_factor_name(self):
        # Because we use the slug as the name for the smallerize.Factor,
        #   it won't match the original name for the factor if it's
        #   capitalized. Make sure we're dealing with this case properly
        factor = FactorFactory(name='Capital', study=self.study)
        levels = LevelFactory.create_batch(2, factor=factor)
        sm_factor = factor.to_smallerize_factor()

        assert sm_factor.name == factor.slug
        assert sm_factor.name == factor.name.lower()

    def test_to_smallerize_nondefault_weight(self):
        factor = FactorFactory(study=self.study, weight=3)
        levels = LevelFactory.create_batch(2, factor=factor)
        sm_factor = factor.to_smallerize_factor()

        assert sm_factor.weight == 3
