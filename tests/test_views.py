import random

import pytest
from django.test import Client, TestCase
from django.contrib.messages import get_messages
from django.shortcuts import reverse

from randomizer.models import Study

from .factories import (UserFactory, StudyFactory, ArmFactory, FactorFactory,
                        LevelFactory, MinimConfigFactory)


class TestLandingView(TestCase):

    def setUp(self):
        self.client = Client()

    def test_page(self):
        resp = self.client.get("/")
        assert resp.status_code == 200
        assert "Welcome to Leveller" in str(resp.content)


class TestStudyDetailView(TestCase):

    def setUp(self):
        super(TestStudyDetailView, self).setUp()
        user = UserFactory()
        self.client = Client()
        self.client.force_login(user)
        self.study, created = Study.objects.get_or_create(name='Study 1')
        self.study.admins.add(user)

    def test_page(self):
        # TODO: See if we can reduce number of queries here
        with self.assertNumQueries(10):
            resp = self.client.get(
                reverse('study_detail', args=[self.study.id])
            )
        assert resp.status_code == 200
        assert 'Study 1' in str(resp.content)

    def test_logged_out_user(self):
        self.client.logout()
        study_url = reverse('study_detail', args=[self.study.id])
        resp = self.client.get(study_url)
        self.assertRedirects(
            resp,
            reverse('login') + '?next=' + study_url
        )
        messages = list(get_messages(resp.wsgi_request))
        message_strs = [str(m) for m in messages]

        assert resp.status_code == 302
        expected_message = "You don't have permission to view this study."
        assert any(expected_message in msg for msg in message_strs)

    def test_non_authorised_user(self):
        new_user = UserFactory()
        self.client.force_login(new_user)
        resp = self.client.get(
            reverse('study_detail', args=[self.study.id])
        )
        messages = list(get_messages(resp.wsgi_request))
        message_strs = [str(m) for m in messages]

        assert resp.status_code == 403
        expected_message = "You don't have permission to view this study."
        assert any(expected_message in msg for msg in message_strs)


class TestStudyCreateView(TestCase):

    def setUp(self):
        super(TestStudyCreateView, self).setUp()
        self.user = UserFactory()
        self.client = Client()
        self.client.force_login(user=self.user)

    def test_study_creation(self):
        study_data = {
            'name': 'TestStudy'
        }
        management_form_data = {
            'arm-TOTAL_FORMS': 2,
            'arm-MIN_NUM_FORMS': 2,
            'arm-MAX_NUM_FORMS': 1000,
            'arm-INITIAL_FORMS': 0
        }
        arm_formset_data = {
            'arm-0-name': 'Arm1',
            'arm-0-description': 'First arm',
            'arm-0-ratio': 1,
            'arm-1-name': 'Arm2',
            'arm-1-description': 'Second arm',
            'arm-1-ratio': 2
        }
        resp = self.client.post(
            "/studies/create/",
            data={**study_data, **management_form_data,
                  **arm_formset_data},
            follow=True
        )
        assert resp.status_code == 200

        study = resp.context_data['object']
        self.assertRedirects(
            resp,
            expected_url='/studies/{id}/'.format(id=study.id)
        )

        arms = study.arms.all()
        assert all(arm.name in ('Arm1', 'Arm2') for arm in arms)


class TestAllocationCreateView(TestCase):

    def setUp(self):
        super(TestAllocationCreateView, self).setUp()
        self.user = UserFactory()
        self.client = Client()
        self.client.force_login(user=self.user)

        self.study = StudyFactory(admins=[self.user])
        factor1 = FactorFactory(study=self.study)
        factor1_levels = [LevelFactory(factor=factor1) for _ in range(3)]
        factor2 = FactorFactory(study=self.study)
        factor2_levels = [LevelFactory(factor=factor2) for _ in range(2)]
        arm1 = ArmFactory(study=self.study)
        arm2 = ArmFactory(study=self.study)
        config = MinimConfigFactory(study=self.study)

    def test_page(self):
        resp = self.client.get(
            reverse('allocate_new', kwargs={'study': self.study.id})
        )

        form = resp.context['form']
        for factor in self.study.factors.all():
            assert factor.name in form.fields
            choice_names = [name for name, text in
                            form.fields[factor.slug].choices]
            for level in factor.levels.all():
                assert level.name in choice_names

    def test_page_capitalized_factor(self):
        """
        Factor name, not slug, gets used as the field name. Make
        sure we're keeping this consistent by checking how
        we treat a capitalized factor name.
        """
        resp = self.client.get(
            reverse('allocate_new', kwargs={'study': self.study.id})
        )

        form = resp.context['form']
        factor1 = self.study.factors.first()
        factor1.name = 'Capitalized'

        field_names_lower = [field.lower() for field in form.fields]
        for factor in self.study.factors.all():
            assert factor.name in form.fields
            assert factor.slug in field_names_lower


    def test_post(self):
        allocation_data = {
            'id_code': 'XYZ',
            'comments': ''
        }
        for factor in self.study.factors.all():
            factor_levels = factor.levels.all()
            random_level = random.choice(factor_levels)
            allocation_data[factor.slug] = random_level

        resp = self.client.post(
            reverse('allocate_new', kwargs={'study': self.study.id}),
            data=allocation_data,
            follow=True
        )
        assert resp.status_code == 200

        created_allocation = self.study.allocations.get(id_code='XYZ')
        assert created_allocation.arm in self.study.arms.all()

        assert 'Participant XYZ successfully assigned' in str(resp.content)
