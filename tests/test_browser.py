import pytest

from django.test import LiveServerTestCase
from selenium.webdriver import Firefox

pytestmark = pytest.mark.skip("Leave browser testing for now")


class TestLandingView(LiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.browser = Firefox()
        cls.browser.implicitly_wait(2)

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super().tearDownClass()

    def test_page(self):
        self.browser.get('{base}/'.format(base=self.live_server_url))
        assert 'Leveller' in self.browser.title
        jumbo = self.browser.find_element_by_class_name("jumbotron")
        assert "Welcome" in jumbo.text
