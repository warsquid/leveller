import random

from django.conf import settings
import factory

from randomizer.models import (Arm, Allocation, Factor, Level, Study,
                               MinimConfig)


def get_fake_words():
    with open('tests/factory_data/study_words.txt') as study_in:
        study_words = [line.strip() for line in study_in if line.strip()]
    return study_words


class ModelFieldLazyChoice(factory.LazyFunction):
    """
    When a model has a set of choices for a field, choose one randomly
    when creating the instance.
    """

    def __init__(self, model_class, field, *args, **kwargs):
        choices = [choice for choice, label
                   in model_class._meta.get_field(field).choices]
        super(ModelFieldLazyChoice, self).__init__(
            function=lambda: random.choice(choices),
            *args, **kwargs
        )


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = settings.AUTH_USER_MODEL

    email = factory.Faker('email')
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')


class StudyFactory(factory.DjangoModelFactory):
    class Meta:
        model = Study

    name = factory.Faker('sentence',
                         nb_words=2,
                         variable_nb_words=False,
                         ext_word_list=get_fake_words())
    status = Study.PLANNING_STATUS

    @factory.post_generation
    def admins(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for admin in extracted:
                self.admins.add(admin)

    @factory.post_generation
    def allocators(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for allocator in extracted:
                self.allocators.add(allocator)


class FactorFactory(factory.DjangoModelFactory):
    class Meta:
        model = Factor

    name = factory.Faker('word')
    description = factory.Faker('text', max_nb_chars=100)


class LevelFactory(factory.DjangoModelFactory):
    class Meta:
        model = Level

    factor = factory.SubFactory(FactorFactory)
    name = factory.Faker('word')
    description = factory.Faker('text', max_nb_chars=50)


class ArmFactory(factory.DjangoModelFactory):
    class Meta:
        model = Arm

    study = factory.SubFactory(StudyFactory)
    name = factory.Faker('word')
    description = factory.Faker('text', max_nb_chars=50)


class AllocationFactory(factory.DjangoModelFactory):
    class Meta:
        model = Allocation

    study = factory.SubFactory(StudyFactory)
    allocated_by = factory.SubFactory(UserFactory)
    id_code = factory.Faker('user_name')
    arm = factory.SubFactory(ArmFactory)

    @factory.post_generation
    def levels(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for level in extracted:
                self.levels.add(level)


class MinimConfigFactory(factory.DjangoModelFactory):
    class Meta:
        model = MinimConfig

    study = factory.SubFactory(StudyFactory)
    d_imbalance_method = ModelFieldLazyChoice(MinimConfig,
                                              'd_imbalance_method')
    total_imbalance_method = ModelFieldLazyChoice(MinimConfig,
                                                  'total_imbalance_method')
    probability_method = ModelFieldLazyChoice(MinimConfig,
                                              'probability_method')

    @factory.lazy_attribute
    def d_max_range(self):
        if self.d_imbalance_method == MinimConfig.OVER_MAX_RANGE_D:
            return random.randint(0, 5)
        else:
            return None

    @factory.lazy_attribute
    def preferred_p(self):
        if self.probability_method in (MinimConfig.BEST_ONLY_PROB,
                                       MinimConfig.BIASED_COIN_PROB):
            return random.uniform(0.5, 1.0)
        else:
            return None

    @factory.lazy_attribute
    def q(self):
        N = self.study.get_n_arms()
        if self.probability_method == MinimConfig.RANK_ALL_PROB:
            return random.uniform(1 / N, 2 / (N - 1))
        else:
            return None

