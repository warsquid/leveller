# Leveller

![Status: Alpha](https://img.shields.io/badge/Status-Alpha-yellow.svg)
![MPL 2.0](https://img.shields.io/badge/License-MPL%202.0-blue.svg)

A webapp for allocating participants in clinical trials, using
the adaptive 
[minimisation](https://en.wikipedia.org/wiki/Minimisation_%28clinical_trials%29)
method to maintain balance.

Leveller is built on top of [smallerize](https://gitlab.com/warsquid/smallerize),
an open-source Python module implementing minimisation. 

Leveller is built using Python 3, Django 2 and Bootstrap 4.

## Features

* Create, configure, and run studies without writing code.
* Users can be assigned as admins, who can configure the study
  and access the full list of allocations, or allocators,
  who can only allocate new participants (users can have
  different roles on each study).
* Export the list of allocations in Excel.

## Technical features

* Tested with `pytest`, using [faker](https://github.com/joke2k/faker) and
  [factory_boy](https://factoryboy.readthedocs.io/en/latest/) to easily
  create testing data with all the required relationships.
* Automatically populate the database for testing/development using the
  `populate_db` command.

## Screenshots

View active studies:

![View all your studies](docs/images/study_list_screenshot.png)

View and configure study settings:

![View/configure a study](docs/images/study_detail_screenshot.png)

Allocating a new participant based on their prognostic factors:

![Allocate a new participant, based on their prognostic factors](docs/images/allocation_screenshot.png)

Confirmation dialog for allocations:

![Allocation requires confirmation](docs/images/allocation_confirmation_screenshot.png)

Minimisation settings:

![Minimisation settings](docs/images/minimisation_settings_screenshot.png)
